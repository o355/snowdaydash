# Snow Day Dashboard

AdminLTE 2-based dashboard for managing Snow Day SMS Subscription

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Snow Day Dashboard Spaghet-o-meter: **3/10**

Given the architecture of the Snow Day Dashboard, the Snow Day Dashboard generally is & is not spaghetti code. Modularity is achieved in individual files. There are global plugins to handle settings, logout, and date parsing. The settings code is in a separate JS file, instead of being inline. 

There is some copypasta code even in the separate JS files, and across the pages. The check for session status should have been a function in a separate file. Some HTML pages do have excessive inline code which could have been moved to separate JS files. There is some reusable code - especially with stopAllAnimation, but this should have been moved to a separate file.

Additionally, responses for status codes could have been made more efficient by wrapping them into a global function.

The copypasta HTML headers & navigation could have been solved by using a framework like React. However, this is standard for AdminLTE-based projects.

# Getting started
To get started with the Snow Day Dashboard, you will need:
* A computer
* A web server to host the dashboard on
* A fully set up instance of the Snow Day API (directions can be found at https://gitlab.com/o355/snowdayapi).
* A setup of LambStatus for the status page box on the homepage to work properly

Download the Snow Day Dashboard by either downloading the repo as a .zip, or using git clone (`git clone https://gitlab.com/o355/snowdaydash.git`). The total size of the dashboard is about 20 MB and has about 8600 files, so this process might take a bit of time.

Once you have a fully running copy of the Snow Day API, you'll want to go to all the .html files, and make modifications where the comments denote such. This is so that your copy of the dashboard can communicate with your copy of the Snow Day API.

Once all of that is complete, your copy of the Snow Day Dashboard should be up and running.

If you make modifications to the error codes that the Snow Day API returns (and that the dashboard relies on), make sure you modify your copy of the Snow Day Dashboard as needed.

# License & 3rd-Party Libraries
The Snow Day Dashboard is licensed under the MIT License. The Snow Day Dashboard includes the AdminLTE 2 framework, which is licensed under the MIT License. The Snow Day Dashboard includes Font Awesome Version 4, which is licensed under the Font Awesome Free License.
