// Date parser to avoid incompatibility issues with Safari and toLocaleString just not working
// Formats are as follows:
// hm - September 1, 2019 at 12:00 PM
// hms - September 1, 2019 at 12:00:00 PM

function dateparser(rawdate, format) {
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    var month = months[rawdate.getMonth()]
    var day = rawdate.getDate()
    var year = rawdate.getFullYear()
    var hour = rawdate.getHours()
    var minute = rawdate.getMinutes()
    var second = rawdate.getSeconds()
    var hourdelim;

    if (hour >= 13) {
        hourdelim = "PM"
        hour = hour - 12
    } else if (hour == 12) {
        hourdelim = "PM"
    } else if (hour <= 11) {
        hourdelim = "AM"
    }

    if (hour == 0 && hourdelim == "AM") {
        hour = 12
    }

    if (minute <= 9) {
        minute = "0" + minute
    }

    if (second <= 9) {
        second = "0" + second
    }

    if (format == "hm") {
        return month + " " + day + ", " + year + " at " + hour + ":" + minute + " " + hourdelim
    } else if (format == "hms") {
        return month + " " + day + ", " + year + " at " + hour + ":" + minute + ":" + second + " " + hourdelim
    } else {
        return
    }
} 