var logout_timeout;
function logOut() {
    $("#logout-href").prop("disabled", true)
    $("#logout-i").prop("class", "fa fa-refresh fa-spin")
    $("#logout-text").html("Signing out...")
    var xhr = new XMLHttpRequest();
    var async = true;
    var method = "POST"
    var url = "https://snowdayapi.owenthe.dev/api/v2/terminateSession"

    xhr.open(method, url, async)
    xhr.withCredentials = true
    xhr.send()
    xhr.timeout = 30000

    xhr.onload = function () {
        var data = xhr.responseText;
        var obj = JSON.parse(data)
        var code = obj['code']
        if (code == "100") {
            $("#logout-href").prop("disabled", false)
            $("#logout-i").prop("class", "fa fa-exclamation-triangle")
            $("#logout-text").html("Sign-out failed, try again later.")
            clearTimeout(logout_timeout)
            logout_timeout = setTimeout(function () {
                $("#logout-i").prop("class", "fa fa-power-off")
                $("#logout-text").html("Sign Out")
            }, 5000)
        } else {
            window.location.replace("https://owenthe.dev/snowday/dash/index.html?ref=losu")
        }
    }

    xhr.onabort = function () {
        $("#logout-href").prop("disabled", false)
        $("#logout-i").prop("class", "fa fa-exclamation-triangle")
        $("#logout-text").html("Sign-out failed, try again later.")
        clearTimeout(logout_timeout)
        logout_timeout = setTimeout(function () {
            $("#logout-i").prop("class", "fa fa-power-off")
            $("#logout-text").html("Sign Out")
        }, 5000)
    }

    xhr.ontimeout = function () {
        $("#logout-href").prop("disabled", false)
        $("#logout-i").prop("class", "fa fa-exclamation-triangle")
        $("#logout-text").html("Sign-out failed, try again later.")
        clearTimeout(logout_timeout)
        logout_timeout = setTimeout(function () {
            $("#logout-i").prop("class", "fa fa-power-off")
            $("#logout-text").html("Sign Out")
        }, 5000)
    }

    xhr.onerror = function () {
        $("#logout-href").prop("disabled", false)
        $("#logout-i").prop("class", "fa fa-exclamation-triangle")
        $("#logout-text").html("Sign-out failed, try again later.")
        clearTimeout(logout_timeout)
        logout_timeout = setTimeout(function () {
            $("#logout-i").prop("class", "fa fa-power-off")
            $("#logout-text").html("Sign Out")
        }, 5000)
    }
}