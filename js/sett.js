// Testing testing 1234

function banner_slide() {
    $('.settings-btn').button('reset')
    $('#settingsbox').slideDown('slow').delay(7500).slideUp('slow')
    window.scrollTo($("#settingsbox").position().left, $("#settingsbox").position().top)
}

function banner_slide_15s() {
    $('.settings-btn').button('reset')
    $('#settingsbox').slideDown('slow').delay(15000).slideUp('slow')
    window.scrollTo($("#settingsbox").position().left, $("#settingsbox").position().top)
}

function stopAllAnimation() {
    $('#settingsbox').stop(true, true).hide()
}

function banner_slide_nobtnreset() {
    $('#settingsbox').slideDown('slow').delay(7500).slideUp('slow')
    window.scrollTo($("#settingsbox").position().left, $("#settingsbox").position().top)
}

// https://stackoverflow.com/a/1144788
function escapeRegExp(str) {
    return str.replace(/([.*?<>^=!:${}()|\[\]\/\\])/g, "");
}

function updateCountdownTime() {
    countdowntime = $("#countdowntext").text()
    if (countdowntime != 0) {
        newtime = countdowntime - 1;
        if (newtime != 1) {
            $("#secondstext").html("seconds")
        } else {
            $("#secondstext").html("second")
        }
        $("#countdowntext").html(newtime)
    }
}

function pms_update(settings_bit) {
    var msgspan = "";
    if (settings_bit == 0) {
        msgspan = "(Snow Day Predictor): New prediction 12/1, 9:00 AM: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 1) {
        msgspan = "(Snow Day Predictor): New prediction 12/1, 9:00 AM EST: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 10) {
        msgspan = "New prediction 12/1, 9:00 AM: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 11) {
        msgspan = "New prediction 12/1, 9:00 AM EST: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 100) {
        msgspan = "(Snow Day Predictor): New prediction 12/1, 09:00: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 101) {
        msgspan = "(Snow Day Predictor): New prediction 12/1, 09:00 EST: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 110) {
        msgspan = "New prediction 12/1, 09:00: Wednesday chance of a snow day 95%"
    } else if (settings_bit == 111) {
        msgspan = "New prediction 12/1, 09:00 EST: Wednesday chance of a snow day 95%"
    }
    $("#predmsg_content").text(msgspan)
}

function pms_onchange() {
    settings_bit = 0;
    if ($("#twentyfour_prefixactive").prop("checked")) {
        settings_bit = settings_bit + 100
    }

    if ($("#noprefix_active").prop("checked")) {
        settings_bit = settings_bit + 10
    } 

    if ($("#timezone_active").prop("checked")) {
        settings_bit = settings_bit + 1
    }

    pms_update(settings_bit)
}

function setDefaultSettings () {
    var xhr = new XMLHttpRequest();
    var async = true
    var method = "POST"
    var url = "https://snowdayapi.owenthe.dev/api/v2/updateUserSettings"

    $("#sds-button").button('loading')

    var formdata = new FormData()
    formdata.append("userGroup", "high")
    formdata.append("messageHold", "0")
    formdata.append("twentyFourPrefix", "off")
    formdata.append("noPrefix", "off")
    formdata.append("timezoneInMsg", "off")
    formdata.append("dashName", "default_dash_name")
    formdata.append("pihomepage", "on")
    formdata.append("pdehomepage", "on")
    formdata.append("sshomepage", "on")
    formdata.append("sqlhomepage", "off")
    formdata.append("amhomepage", "on")

    xhr.open(method, url, async)
    xhr.withCredentials = true
    xhr.send(formdata)
    xhr.timeout = 30000

    xhr.onload = function () {
        var data = xhr.responseText;
        var obj = JSON.parse(data)
        var code = obj['code'].toString()
        if (code == "129") {
            stopAllAnimation()
            $('#modal-sds').modal('toggle')
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "Session ID was not specified, likely due to an expired session. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "100") {
            stopAllAnimation()
            $('#modal-sds').modal('toggle')
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "A bad origin error occurred. This could be because your browser and/or computer are compromised. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "128" || code == "125") {
            stopAllAnimation()
            $('#modal-sds').modal('toggle')
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "Session ID has expired. Please log back in to change your settings. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "133") {
            stopAllAnimation()
            $('#modal-sds').modal('toggle')
            $("#sds-button").button("reset")
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "A server-side error occurred while trying to save your settings. Try again later, or refresh the page (all pending changes will be lost)."
            banner_slide_nobtnreset()
        } else if (code == "59") {
            stopAllAnimation()
            $('#modal-sds').modal('toggle')
            $("#sds-button").button("reset")
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "The API is down for maintenance. Check <a href='https://status.owenthe.dev' target='_blank'>https://status.owenthe.dev</a> for more information, then try again when the API is online."
            banner_slide_nobtnreset()
        } else if (code == "68") {
            stopAllAnimation()
            $("#modal-sds").modal("toggle")
            $("#sds-button").button("reset")
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Failed to change settings to default"
            document.getElementById("settingsbox_text").innerHTML = "You are making too many requests to update settings, and you have been rate limited. Please wait for a minute, then try again. If this keeps happening, contact support."
            banner_slide_nobtnreset()
        } else if (code == "140") {
            $('#modal-sds').modal('toggle')
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-success")
            document.getElementById("settingsbox_header").innerHTML = "Default settings saved successfully!"
            document.getElementById("settingsbox_text").innerHTML = "All settings have been changed to their defaults. Refreshing page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            document.getElementById("")
            setTimeout(banner_slide, 150)
            setInterval(updateCountdownTime, 1000)
            setTimeout(function () {window.location.replace("https://owenthe.dev/snowday/dash/settings.html")}, 5000)
        } else {
            $('#modal-sds').modal('toggle')
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "An unknown status code was returned from the API. Try again later, or refresh the page (all pending changes will be lost)."
            setTimeout(banner_slide, 150)
        }
    }

    xhr.onerror = function () {
        stopAllAnimation()
        $('#modal-sds').modal('toggle')
        document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
        document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
        document.getElementById("settingsbox_text").innerHTML = "An error occurred while querying the API. This may be due to your internet connection being offline. Try again later, and if this keeps happening, contact support."
        banner_slide()
    }
    
    xhr.ontimeout = function () {
        stopAllAnimation()
        $('#modal-sds').modal('toggle')
        document.getElementById("settingsbox").setAttribute("class", "callout callout-warning")
        document.getElementById("settingsbox_header").innerHTML = "Settings couldn't be saved"
        document.getElementById("settingsbox_text").innerHTML = "The request to update your settings timed out. Make sure you have a working internet connection that doesn't block the API, then try again. You can also try again later, or refresh the page (all pending changes will be lost)."
        banner_slide()
    }
}

function submit_form (form_id){
    var xhr = new XMLHttpRequest();
    var async = true
    var method = "POST";
    var url = "https://snowdayapi.owenthe.dev/api/v2/updateUserSettings"

    $('.settings-btn').button('loading')

    if (form_id == "#mc_form") {
        if (document.getElementById("twentyfour_prefixactive").checked) {
            document.getElementById("twentyfour_prefixdisabled").disabled = true;
        } else {
            document.getElementById("twentyfour_prefixdisabled").disabled = false;
        }

        if (document.getElementById("noprefix_active").checked) {
            document.getElementById("noprefix_disabled").disabled = true;
        } else {
            document.getElementById("noprefix_disabled").disabled = false;
        }

        if (document.getElementById("timezone_active").checked) {
            document.getElementById("timezone_disabled").disabled = true;
        } else {
            document.getElementById("timezone_disabled").disabled = false; 
        }
    }

    if (form_id == "#hm_form") {
        if (document.getElementById("holdactive").checked) {
            document.getElementById("holdactive_hidden").disabled = true;
        } else {
            document.getElementById("holdactive_hidden").disabled = false;
        }
    }

    if (form_id == "#dash_form") {
        if ($("#pihomepage_active").prop("checked")) {
            $("#pihomepage_disabled").attr("disabled", true)
        } else {
            $("#pihomepage_disabled").attr("disabled", false)
        }

        if ($("#pdehomepage_active").prop("checked")) {
            $("#pdehomepage_disabled").attr("disabled", true)
        } else {
            $("#pdehomepage_disabled").attr("disabled", false)
        }

        if ($("#sshomepage_active").prop("checked")) {
            $("#sshomepage_disabled").attr("disabled", true)
        } else {
            $("#sshomepage_disabled").attr("disabled", false)
        }

        if ($("#sqlhomepage_active").prop("checked")) {
            $("#sqlhomepage_disabled").attr("disabled", true)
        } else {
            $("#sqlhomepage_disabled").attr("disabled", false)
        }

        if ($("#amhomepage_active").prop("checked")) {
            $("#amhomepage_disabled").attr("disabled", true)
        } else {
            $("#amhomepage_disabled").attr("disabled", false)
        }
    }

    form_id = form_id.replace("#", "")

    var formdata = new FormData(document.getElementById(form_id))

    xhr.open(method, url, async)
    xhr.withCredentials = true
    xhr.send(formdata)
    xhr.timeout = 30000

    xhr.onload = function() {
        var data = xhr.responseText;
        var obj = JSON.parse(data)
        var code = obj['code'].toString()
        var msg = obj['message'].toString()
        if (form_id == "dash_form") {
            if (document.getElementById("dashName-field").value == "default_dash_name") {
                document.getElementById("dashName-field").value = ""
            } else {
                returned_val = escapeRegExp(document.getElementById("dashName-field").value)
                $("#dashName-field").val(returned_val)
            }
        }
        if (code == "129") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "Session ID was not specified, likely due to an expired session. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "100") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "A bad origin error occurred. This could be because your browser and/or computer are compromised. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "128" || code == "125") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "Session ID has expired. Please log back in to change your settings. Redirecting to sign-in page in <span id='countdowntext'>5</span> <span id='secondstext'>seconds</span>..."
            banner_slide_nobtnreset()
            setInterval(updateCountdownTime, 1000)
            setTimeout(function() {window.location.replace("https://owenthe.dev/snowday/dash/index.html")}, 5000)
        } else if (code == "59") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "The API is down for maintenance. Check <a href='https://status.owenthe.dev' target='_blank'>https://status.owenthe.dev</a> for more information, then try again when the API is online."
            banner_slide()
        } else if (code == "68") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "You are making too many requests to update settings, and you have been rate limited. Please wait for a minute, then try again. If this keeps happening, contact support."
            banner_slide()
        } else if (code == "133") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "A server-side error occurred while trying to save your settings. Try again later, or refresh the page (all pending changes will be lost)."
            banner_slide()
        } else if (code == "139") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-warning")
            document.getElementById("settingsbox_header").innerHTML = "Settings couldn't be saved"
            document.getElementById("settingsbox_text").innerHTML = "The dashboard name you specified was in excess of 64 characters. Please set your dashboard name to 63 characters or less, then try again."
            banner_slide()
        } else if (code == "147") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-warning")
            document.getElementById("settingsbox_header").innerHTML = "Settings couldn't be saved"
            document.getElementById("settingsbox_text").innerHTML = "The outbound SMS number you selected is currently at capacity. Please choose a number that has capacity."
            banner_slide()
            if (form_id == "smear_form") {
                if (original_outboundNum == "+18452456252") {
                    $("#on-1").prop("checked", true)
                } else if (original_outboundNum == "+18452456118") {
                    $("#on-2").prop("checked", true)
                }
                getCapacityStatus()
            }
        } else if (code == "140" || code == "149") {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-success")
            if (form_id == "smear_form") {
                document.getElementById("settingsbox_header").innerHTML = "Settings saved successfully!"
                document.getElementById("settingsbox_text").innerHTML = "The outbound SMS number has been updated. Make sure that the outbound number you picked is unblocked on your phone. If you previously texted STOP or UNSUBSCRIBE to the outbound number you picked, make sure to text back START so that you don't encounter message delivery errors."
                getCapacityStatus()
                original_outboundNum = $("input[name=outboundNum]").val()
                banner_slide_15s()
            } else {
                document.getElementById("settingsbox_header").innerHTML = "Settings saved successfully!"
                document.getElementById("settingsbox_text").innerHTML = "Any changes you made on this tab have been saved."
                if (document.getElementById("dashName-field").value == "") {
                    $("#dashName-field").val("Loading...")
                    document.getElementById("welcometext").innerHTML = "Welcome"
                    getUserSettings()
                } else {
                    document.getElementById("welcometext").innerHTML = "Welcome, " + document.getElementById("dashName-field").value + ","
                }
                banner_slide()
            }
        } else {
            stopAllAnimation()
            document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
            document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
            document.getElementById("settingsbox_text").innerHTML = "An unknown status code was returned from the API. Try again later, or refresh the page (all pending changes will be lost)."
            banner_slide()
        }
    }

    xhr.onerror = function () {
        stopAllAnimation()
        document.getElementById("settingsbox").setAttribute("class", "callout callout-danger")
        document.getElementById("settingsbox_header").innerHTML = "Settings failed to save successfully"
        document.getElementById("settingsbox_text").innerHTML = "An error occurred while querying the API. This may be due to your internet connection being offline. Try again later, and if this keeps happening, contact support."
        banner_slide()
    }
    
    xhr.ontimeout = function () {
        stopAllAnimation()
        document.getElementById("settingsbox").setAttribute("class", "callout callout-warning")
        document.getElementById("settingsbox_header").innerHTML = "Settings couldn't be saved"
        document.getElementById("settingsbox_text").innerHTML = "The request to update your settings timed out. Make sure you have a working internet connection that doesn't block the API, then try again. You can also try again later, or refresh the page (all pending changes will be lost)."
        banner_slide()
    }

}

function updateHoldDates(starting_time){
    if (starting_time == 0) {
        var current_date = new Date()
        var current_unix = current_date.getTime() / 1000
        onedaydate = new Date(current_date.getTime() + 86400000)
        threedaydate = new Date(current_date.getTime() + 259200000)
        sevendaydate = new Date(current_date.getTime() + 604800000)
        fourteendaydate = new Date(current_date.getTime() + 1209600000)
        thirtydaydate = new Date(current_date.getTime() + 2592000000)
        sixtydaydate = new Date(current_date.getTime() + 5184000000)
        $("#radio-hm-1d").val(current_unix + 86400)
        $("#radio-hm-3d").val(current_unix + 259200)
        $("#radio-hm-7d").val(current_unix + 604800)
        $("#radio-hm-14d").val(current_unix + 1209600)
        $("#radio-hm-30d").val(current_unix + 2592000)
        $("#radio-hm-60d").val(current_unix + 5184000)

    } else {
        current_date = new Date(starting_time * 1000)
        current_unix = new Date()
        current_unix = current_unix.getTime() / 1000

        onedaydate = new Date(current_date.getTime() + 86400000)
        threedaydate = new Date(current_date.getTime() + 259200000)
        sevendaydate = new Date(current_date.getTime() + 604800000)
        fourteendaydate = new Date(current_date.getTime() + 1209600000)
        thirtydaydate = new Date(current_date.getTime() + 2592000000)
        sixtydaydate = new Date(current_date.getTime() + 5184000000)

        if ($("#radio-hm-1d").val() > current_unix + 5184000) {
            $("#radio-hm-1d").attr("disabled", true)
        } else {
            $("#radio-hm-1d").attr("disabled", false)
        }

        if ($("#radio-hm-3d").val() > current_unix + 5184000) {
            $("#radio-hm-3d").attr("disabled", true)
        } else {
            $("#radio-hm-3d").attr("disabled", false)
        }

        if ($("#radio-hm-7d").val() > current_unix + 5184000) {
            $("#radio-hm-7d").attr("disabled", true)
        } else {
            $("#radio-hm-7d").attr("disabled", false)
        }

        if ($("#radio-hm-14d").val() > current_unix + 5184000) {
            $("#radio-hm-14d").attr("disabled", true)
        } else {
            $("#radio-hm-14d").attr("disabled", false)
        }

        if ($("#radio-hm-30d").val() > current_unix + 5184000) {
            $("#radio-hm-30d").attr("disabled", true)
        } else {
            $("#radio-hm-30d").attr("disabled", false)
        }

        if ($("#radio-hm-60d").val() > current_unix + 5184000) {
            $("#radio-hm-60d").attr("disabled", true)
        } else {
            $("#radio-hm-60d").attr("disabled", false)
        }
    }

    document.getElementById("hold-messages-1-day-span").innerHTML = dateparser(onedaydate, "hm")
    document.getElementById("hold-messages-3-day-span").innerHTML = dateparser(threedaydate, "hm")
    document.getElementById("hold-messages-7-day-span").innerHTML = dateparser(sevendaydate, "hm")
    document.getElementById("hold-messages-14-day-span").innerHTML = dateparser(fourteendaydate, "hm")
    document.getElementById("hold-messages-30-day-span").innerHTML = dateparser(thirtydaydate, "hm")
    document.getElementById("hold-messages-60-day-span").innerHTML = dateparser(sixtydaydate, "hm")
}